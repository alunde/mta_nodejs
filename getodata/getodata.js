"use strict";
var express = require("express");
var request = require("request");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = function(){
	var app = express(); 
	
// /getodata/ Router
	// 
	app.route("/")	// /getodata/
	  .get(function(req, res) {
	    res.send("Hello Get ODATA World Node.js");
	});
	
	// Note the for this service URL detail discovery to work you must define a user provided service
	// xs cups sapcp-http-odata -p '{"url":"https://sfp00maind0cd77f9b.us2.hana.ondemand.com/http/odata/http.xsodata/","type":"SAPCP"}'

	app.route("/get")	// /getodata/get
	  .get(function(req, res) {
		var svcs = JSON.parse(process.env.VCAP_SERVICES);
		var creds = svcs["user-provided"][0].credentials;
		var url = creds.url;

		console.log("HTTP ODATA URL = " + url + "");

		request({
			url: url + "connection/",
			method: "GET",
			headers: {
				"Accept": "application/json"
			}
		}, function(error, response, body) {
			if (response.statusCode == 200) {
				//console.log("Body:\n" + body + "\n");
				var jb = JSON.parse(body);
				if (jb.d.results.length > 0) {
					for(var i=0; i < jb.d.results.length; i++) {
						var conn = jb.d.results[i];
						var method = conn.method;
						var body = conn.resbody;
						console.log("Conn[" + i + "] " + method + " " + body + "\n");
					}
				} else {
					console.log("No Connection Data");
				}
			} else {
				console.log(error);
			}

		});

	    res.send("Get ODATA URL : " + url);
	});
	

	//Simple Database Select - Async Waterfall
	app.route("/dummy2")	// /getodata/dummy2
		.get(function(req, res) {
			var client = req.db;
			async.waterfall([
				function prepare(callback) {
					client.prepare("select SESSION_USER from \"mta_nodejs.db::Dummy\" ",
						function(err, statement) {
							callback(null, err, statement);
						});
				},
				function execute(err, statement, callback) {
					statement.exec([], function(execErr, results) {
						callback(null, execErr, results);
					});
				},
				function response(err, results, callback) {
					if (err) {
						res.type("text/plain").status(500).send("ERROR: " + err);
					} else {
						var result = JSON.stringify({
							Objects: results
						});
						res.type("application/json").status(200).send(result);
					}
					callback();
				}
			]);
		});

	
   return app;	
};