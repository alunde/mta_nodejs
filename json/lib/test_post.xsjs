$.response.contentType = "text/html";
var body = "";

body += "<html>\n";
body += "<head>\n";
body += "\n";
body += "<script>\n";
body += "\n";
body += "\n";
body += "function randomIntFromInterval(min,max)\n";
body += "{\n";
body += "    return Math.floor(Math.random()*(max-min+1)+min);\n";
body += "}\n";
body += "var request_output = \"\";\n";
body += "\n";
body += "var xhr = new XMLHttpRequest();\n";
body += "xhr.open('POST', 'server.xsjs');\n";
body += "request_output += \"POST to server.xsjs \\n\";\n";
body += "xhr.setRequestHeader('Content-Type', 'application/json');\n";
body += "request_output += \"Request Header: Content-Type = application/json \\n\";\n";
body += "xhr.setRequestHeader('Accept', 'application/json');\n";
body += "request_output += \"Request Header: Accept = application/json \\n\";\n";
body += "\n";
body += "request_output += \"\\n\";\n";
body += "request_output += \"Request Body: \\n\";\n";
body += "\n";
body += "function dumpResponseHeaders(xhr_obj) {\n";
body += "    var hdr_out = \"\";\n";
body += "    hdr_out += \"Response Headers\\n\";\n";
body += "    hdr_out += \"\\n\";\n";
body += "    var headers = xhr_obj.getAllResponseHeaders();\n";
body += "    return (hdr_out + headers + \"\\n\");\n";
body += "}\n";
body += "xhr.onload = function() {\n";
body += "    if (xhr.status === 201) {\n";
body += "        document.getElementById(\"response_content\").innerHTML = \"<pre>\\n\" + dumpResponseHeaders(xhr) + \"Response Body: \\n\" + JSON.stringify(JSON.parse(xhr.responseText), null, 2) + \"</pre>\\n\";\n";
body += "    }\n";
body += "    else {\n";
body += "        alert(\"Yipe!  Got status \" + xhr.status + \" : \" + xhr.statusText + \" ::\\n \" + JSON.parse(xhr.responseText).error.message.value + \" \\n:: expected 201.\");\n";
body += "    }\n";
body += "};\n";
body += "\n";
body += "var request_obj = \n";
body += "{ \n";
body += "       //\"connId\": 0, \n";
body += "       \"status\": \"\", \n";
body += "       \"rescode\": 200, \n";
body += "       \"failure\": \"\", \n";
body += "       \"proto\": \"http\", \n";
body += "       \"user\": \"user\", \n";
body += "       \"pass\": \"pass\", \n";
body += "       \"host\": \"xsadv.sfpchp.com\", \n";
body += "       \"port\": 80, \n";
body += "       \"path\": \"/\", \n";
body += "       \"query\": \"\", \n";
body += "       \"frag\": \"\", \n";
body += "       \"chost\": \"0.0.0.0\", \n";
body += "       \"cport\":1080, \n";
body += "       \"reqstart\": new Date().toISOString(), \n";
body += "       \"reqend\": new Date().toISOString(), \n";
body += "       \"resstart\": new Date().toISOString(), \n";
body += "       \"resend\": new Date().toISOString(), \n";
body += "       \"cconnest\": \"na\", \n";
body += "       \"sconninit\": \"na\", \n";
body += "       \"sconnhshake\": \"na\", \n";
body += "       \"req\": { \n";
body += "           //\"reqId\": 0, \n";
body += "           //\"parent\": 0, \n";
body += "           \"method\": \"POST\", \n";
body += "           \"headers\": [ \n";
body += "               { \"name\": \"reqHDR1\", \"value\": \"VALUE1\" }, \n";
body += "               { \"name\": \"reqHDR2\", \"value\": \"VALUE2\" }, \n";
body += "               { \"name\": \"reqHDR3\", \"value\": \"VALUE3\" } \n";
body += "           ], \n";
body += "           \"body\": \"{ POST Request Body }\" \n";
body += "       }, \n";
body += "       \"res\": { \n";
body += "           //\"resId\": 0, \n";
body += "           //\"parent\": 0, \n";
body += "           \"headers\": [ \n";
body += "               { \"name\": \"resHDR1\", \"value\": \"VALUE1\" }, \n";
body += "               { \"name\": \"Content-Type\", \"value\": \"text/html\" } \n";
body += "           ], \n";
body += "           \"body\": \"<html><body>Response Body</body></head>\" \n";
body += "       } \n";
body += "}; \n";



body += "\n";
body += "</script>\n";
body += "\n";
body += "</head>\n";
body += "<body>\n";
body += "<p>===Begin Request===</p>\n";
body += "<div id=\"request_content\">\n";
body += "No request sent yet.    \n";
body += "</div>\n";
body += "<p>===End Request===</p>\n";
body += "<p>===Begin Response===</p>\n";
body += "<div id=\"response_content\">\n";
body += "No results returned yet.    \n";
body += "</div>\n";
body += "<p>===End Response===</p>\n";
body += "\n";
body += "<script>\n";
body += "// Display the request\n";
body += "document.getElementById(\"request_content\").innerHTML = \"<pre>\\n\" + request_output + \"\\n\" + JSON.stringify(request_obj, null, 2) + \"</pre>\\n\";\n";
body += "\n";
body += "// Send the Ajax Request.\n";
body += "xhr.send(JSON.stringify(request_obj));\n";
body += "</script>\n";
body += "\n";

body += "</body>\n";
body += "</html>\n";

$.response.setBody(body);
