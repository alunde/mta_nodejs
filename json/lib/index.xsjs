$.response.contentType = "text/html";
var body = "";

body += "<html>\n";
body += "<head>\n";
body += "</head>\n";
body += "<body>\n";
body += "<p>HTTP ODATA  Application</p>\n";
body += "<p>JSON Folder</p>\n";

body += "<a href=\"server.xsjs?json={}\" target=\"test\">Using Get with empty JSON will INSERT a built in test connection structure.</a><br />\n";

body += "<a href=\"test_post.xsjs\" target=\"test\">Test Post a local connection structure.</a> missing privilege CREATE TEMPORARY TABLE for SCHEMA VB6C8WFFMXRCCMRT_MTA_NODEJS_HDI_CONTAINER<br />\n";

body += "\n";

body += "<p>Docs:</p>\n";

body += "<a href=\"https://help.sap.com/doc/3de842783af24336b6305a3c0223a369/2.0.00/en-US/index.html\">SAP HANA XS JavaScript API Reference</a><br />\n";

body += "<a href=\"http://help-legacy.sap.com/saphelp_hanaplatform/helpdata/en/c6/bbca35b7734168ac585c0aef9bb527/content.htm?frameset=/en/d3/c53d059efb40a491e07fc93c063a14/frameset.htm&current_toc=/en/34/29fc63a1de4cd6876ea211dc86ee54/plain.htm&node_id=328&show_children=false\">Using the Server-Side JavaScript APIs</a><br />\n";


body += "</body>\n";
body += "</html>\n";

$.response.setBody(body);
